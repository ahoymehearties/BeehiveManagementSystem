﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeehiveManagementSystem
{
    internal class EggCare : Bee
    {
        public const float CARE_PROGRESS_PER_SHIFT = 0.15f;

        // Declare a Queen variable, undefined
        private Queen queen;
        public override float CostPerShift { get { return 1.35f; } }

        // Call the base constructor with the "Egg Care" job passed as an argument
        // The EggCare type requires a Queen, and we make it the queen we declared above.
        // This is just a placeholder, I think. The Queen class calls the constructor...
        // And then it references "this" to refer to itself as the queen parameter.
 
    
        public EggCare(Queen queen) : base("Egg Care") {
            this.queen = queen;
        }

        protected override void DoJob()
        {
            // The job here is for the QUEEN to take care of the eggs??
            // The queen is the holder of the eggs, so I guess
            // Theoretically we could say this is asking for PERMISSION
            // to care fo the eggs. I guess. This exercise is confusing as hell.
            queen.CareForEggs(CARE_PROGRESS_PER_SHIFT);
        }
    }
}
