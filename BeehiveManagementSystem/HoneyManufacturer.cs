﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeehiveManagementSystem
{
    internal class HoneyManufacturer : Bee
    {
        public const float NECTAR_PROCESSED_PER_SHIFT = 33.15f;
        public override float CostPerShift { get { return 1.7f; } }

        // Constructor for the honey manufacturer. REMEMBER. 
        // This is using the BASE constructor, invoked with : base
        // It's the same as calling Bee()
        // With a string passed of "Honey Manufacturer
        // Therefore the actual method sets Bee.Job = "Honey Manufacturer"
        // It's not that hard but it's really shitty logic to read.
        // What if I'm two bases away?!
        // Well, that's impossible
        // I can only inherit from one actual class, abstract or otherwise
        // But I can inherit a million interfaces
        // And interfaces don't have constructors. 
        // Therefore there can only be one base. 
        // "Base:" therefore is always ONE class upward.
        public HoneyManufacturer() : base("Honey Manufacturer") { }

        // All classes must define the DoJob method declared in Bee.
        protected override void DoJob()
        {
            HoneyVault.ConvertNectarToHoney(NECTAR_PROCESSED_PER_SHIFT);
        }
    }
}
