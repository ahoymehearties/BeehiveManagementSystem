﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeehiveManagementSystem
{
    /* This is an abstract class. Every member below must be defined
     * by its inheritors.
     */

    abstract class Bee : IWorker
    {
        /* I'm the Bee class! I define things about ALL bees. */

        // This is the public field for the bee's job.
        // The Interface IWorker mandates that we have this property.
        public string Job { get; private set; }

        // All Bees will have their own cost per shift, so this is virtual.
        public abstract float CostPerShift { get; }

        // This is a Bee constructor. It just has a job string passed that
        // defined the bee's Job field, declared above.

        public Bee(string job)
        {
            this.Job = job;
        }

        // This is a method for the bee to work a shift. 
        // The interface mandates that we have this method.

        public void WorkTheNextShift()
        {
            /* Working backwards.
             * We are determining whether the bee has enough honey
             * to do its job. We use the ConsumeHoney bool method we wrote
             * in HoneyVault, and pass in the float of CostPerShift.
             * This will be determined by sublasses, and it is 
             * the cost of an individual bee to do its shift.
             * If it is TRUE, ie, the bank has enough honey(costpershift), 
             * we let the bee do its job. */

            if (HoneyVault.ConsumeHoney(CostPerShift))
            {
                DoJob();
            }
        }

        /* This is a DoJob function. It's empty, because the bees
         * that inherit this class will define it themselves. This
         * is something close to an abstract class, except in an abstract
         * class, the method MUST be defined by inheritors. I guess this
         * could be abstract, but let's not get ambitious. */

        protected abstract void DoJob();

        
    }
}
