﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeehiveManagementSystem
{
    internal static class HoneyVault
    {
        /* I am the Honey Vault! I hold two things:
         * 1. Honey -- this is the currency of the bees. They must spend honey to do things.
         * 2. Nectar -- this is what bees must collect. It must be processed to become honey.
         * Let's start with two constants.
         */

        // The first is the rate at which nectar is converted.
        // The second is a warning if the level gets too low. Of honey, I guess.
        public const float NECTAR_CONVERSION_RATIO = .19f;
        public const float LOW_LEVEL_WARNING = 10f;


        // The next two private pieces of data are the amount of honey and nectar.
        // These are mutable, but I control these numbers.
        private static float currentHoney = 25f;
        private static float currentNectar = 100f;

        // Public statusreport string with only a getter.
        public static string StatusReport
        {
            get
            {
                string status = $"{currentHoney:0.0} units of honey\n" +
                    $"{currentNectar:0.0} units of nectar";
                string warnings = "";
                if (currentHoney < LOW_LEVEL_WARNING) warnings +=
                        "\nLOW HONEY - ADD A MANUFACTURER";
                if (currentNectar < LOW_LEVEL_WARNING) warnings +=
                        "\nLOW NECTAR - ADD A COLLECTOR";
                return status + warnings;
            }
        }

        // METHODS

        public static void ConvertNectarToHoney(float amount)
        {
            if (amount > currentNectar)
            { amount = currentNectar; }
            // Remove nectar from bank to be converted
            currentNectar -= amount;

            // Convert the nectar
            currentHoney += (amount * NECTAR_CONVERSION_RATIO);
        }

        // The below method determines whether or not honey can be consumed.
        public static bool ConsumeHoney(float amount)
        {
            if (currentHoney >= amount)
            {
                currentHoney -= amount;
                return true;
            }
            return false;
        }

        // Method to passed to by nectar collectors.
        public static void CollectNectar(float amount)
        {
            if (amount > 0f)
            {
                currentNectar += amount;
            }
        }

        

    }
}
