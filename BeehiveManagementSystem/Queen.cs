﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace BeehiveManagementSystem
{
    internal class Queen : Bee, INotifyPropertyChanged
    {
        public const float EGGS_PER_SHIFT = 0.45f;
        public const float HONEY_PER_UNASSIGNED_WORKER = 0.5f; 
        
        /* We override the master Bee class's CostPerShift */
        public override float CostPerShift { get { return 2.15f; } }

        // Initialize a status report field to hold the HoneyVault status report
        public string StatusReport { get; private set; }
        

        // We initialize a Bee array of the queen's workers.

        private IWorker[] workers = new IWorker[0];
        private float eggs = 0;
        private float unassignedWorkers = 4;

        public event PropertyChangedEventHandler? PropertyChanged;

        protected void OnPropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }

        // Constructor
        /* I need to understand this base thing. When the base class
           has a constructor, my sublass needs to call it. Ok. Let's see.
           I think what I'm doing with this constructor is calling
           Bee(string job). So when we say public Queen() : base("Queen")
           We create a new Queen by using Bee("Queen"). Yeah!!
           That's why it's a string here. :) God this shit is hard.*/
        public Queen() : base("Queen")
        {
            /* When we create a queen, we also use the AssignBee
             * function three times, which adds Bee references into the
             * workers array. */
            AssignBee("Nectar Collector");
            AssignBee("Honey Manufacturer");
            AssignBee("Egg Care");
            StatusReport = "";
        }

        // Methods

        protected override void DoJob()
        {
            // Each shift, squeeze out some eggs
            eggs += EGGS_PER_SHIFT;
            // Each Bee in the workers array does its shift
            
            foreach (IWorker worker in workers)
            {
                worker.WorkTheNextShift();
            }
            // We consume honey for the non-working workers
            HoneyVault.ConsumeHoney(unassignedWorkers * HONEY_PER_UNASSIGNED_WORKER);
            UpdateStatusReport();
        }

        // We add a worker below, taking the argument of a Bee class object
        // Or any of its inheritors
        // Theoretically we could add a "Bee bee" but it wouldn't do anything
        private void AddWorker(IWorker worker)
        {
            if (unassignedWorkers >= 1)
            {
                // Decrement the unassigned workers, because we usin' 'em
                unassignedWorkers--;
                /* Resize the array of 'worker', and we make it one unit larger */
                Array.Resize(ref workers, workers.Length + 1);
                /* For the workers Bee[] array, we set the  
                 * last array index to the new worker. It's n-1 because of 0 index.*/
                workers[workers.Length - 1] = worker;
                
            }
        }
        public void AssignBee(string job)
        {
            switch (job)
            {
                case "Nectar Collector":
                    AddWorker(new NectarCollector());
                    break;
                case "Honey Manufacturer":
                    AddWorker(new HoneyManufacturer());
                    break;
                case "Egg Care":
                    //Calls the EggCare bee constructor, using 
                    // "this" as the queen Argument.
                    // "This" meaning this class.
                    AddWorker(new EggCare(this));
                    break;
            }
            UpdateStatusReport();

        }

        public void UpdateStatusReport()
        {
            StatusReport = $"Bzzz!! Reporting in:\n{HoneyVault.StatusReport}\n" +
            $"\nEggs pooped out: {eggs:0.0}\nUnassigned workers: {unassignedWorkers:0.0}\n" +
            $"{WorkerStatus("Nectar Collector")}\n{WorkerStatus("Honey Manufacturer")}\n" +
            $"{WorkerStatus("Egg Care")}\nTotal Workers: {workers.Length}";
            OnPropertyChanged("StatusReport");
        }

        public void CareForEggs(float eggsToConvert)
        {
            if (eggs >= eggsToConvert)
            {
                eggs -= eggsToConvert;
                unassignedWorkers += eggsToConvert;
            }
        }

        private string WorkerStatus(string job)
        {
            int count = 0;
            foreach (Bee worker in workers)
                if (worker.Job == job) count++;
            string s = "s";
            if (count == 1) s = "";
            return $"{count} {job} bee{s}";
        }
    }
}
