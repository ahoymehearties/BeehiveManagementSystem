﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BeehiveManagementSystem
{
    /* This interface is inherited by the Bee class.
     * It mandates that every Bee has a job property
     * It mandates that every Bee has a WorkTheNextShift method
     */

    internal interface IWorker
    {
        public string Job { get; }

        public void WorkTheNextShift()
        {
        }
    }
}
